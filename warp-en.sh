#!/usr/bin/env bash
export PATH=$PATH:/usr/local/bin

red(){
    echo -e "\033[31m\033[01m$1\033[0m"
}
green(){
    echo -e "\033[32m\033[01m$1\033[0m"
}
yellow(){
    echo -e "\033[33m\033[01m$1\033[0m"
}
blue(){
    echo -e "\033[36m\033[01m$1\033[0m"
}
white(){
    echo -e "\033[1;37m\033[01m$1\033[0m"
}

bblue(){
    echo -e "\033[1;34m\033[01m$1\033[0m"
}

rred(){
    echo -e "\033[1;35m\033[01m$1\033[0m"
}


	if [[ -f /etc/redhat-release ]]; then
		release="Centos"
	elif cat /etc/issue | grep -q -E -i "debian"; then
		release="Debian"
	elif cat /etc/issue | grep -q -E -i "ubuntu"; then
		release="Ubuntu"
	elif cat /etc/issue | grep -q -E -i "centos|red hat|redhat"; then
		release="Centos"
	elif cat /proc/version | grep -q -E -i "debian"; then
		release="Debian"
	elif cat /proc/version | grep -q -E -i "ubuntu"; then
		release="Ubuntu"
	elif cat /proc/version | grep -q -E -i "centos|red hat|redhat"; then
		release="Centos"
    fi

if [ $release = "Centos" ]
	then
            red "This script not supported for CentOS!!!!!"
      exit 1
	   fi
 
	if ! type curl >/dev/null 2>&1; then
	   yellow "Detected curl isn't installed, installing..."
           apt update && apt install curl -y 
           else
           green "Detected curl has already installed"
fi

        if ! type wget >/dev/null 2>&1; then
           yellow "Detected wget isn't installed, installing..."
           apt update && apt install wget -y 
           else
           green "Detected wget has already installed"
fi  
	   
bit=`uname -m`
version=`uname -r | awk -F "-" '{print $1}'`
main=`uname  -r | awk -F . '{print $1 }'`
minor=`uname -r | awk -F . '{print $2}'`
rv4=`ip a | grep global | awk 'NR==1 {print $2}' | cut -d'/' -f1`
rv6=`ip a | grep inet6 | awk 'NR==2 {print $2}' | cut -d'/' -f1`
op=`hostnamectl | grep -i Operating | awk -F ':' '{print $2}'`
vi=`hostnamectl | grep -i Virtualization | awk -F ':' '{print $2}'`


yellow "VPS related information is as follows:"
    white "------------------------------------------"
    blue " Operating system name -$op "
    blue " System kernel version - $version " 
    blue " CPU architecture name  - $bit "
    blue " Virtual architecture type -$vi "
    white " -----------------------------------------------" 
sleep 1s

warpwg=$(systemctl is-active wg-quick@wgcf)
case ${warpwg} in
active)
     WireGuardStatus=$(green "Running")
     ;;
*)
     WireGuardStatus=$(red "Stopped")
esac


v44=`ping ipv4.google.com -c 1 | grep received | awk 'NR==1 {print $4}'`

if [[ ${v44} == "1" ]]; then
 v4=`wget -qO- -4 ip.gs` 
 WARPIPv4Status=$(curl -s4 https://www.cloudflare.com/cdn-cgi/trace | grep warp | cut -d= -f2) 
 case ${WARPIPv4Status} in 
 on) 
 WARPIPv4Status=$(green "WARP is turned on, current IPV4 address：$v4 ") 
 ;; 
 off) 
 WARPIPv4Status=$(yellow "WARP isn't turned on, current IPV4 address：$v4 ") 
 esac 
else
WARPIPv4Status=$(red "IPV4 address does not exist")

 fi 

v66=`ping ipv6.google.com -c 1 | grep received | awk 'NR==1 {print $4}'`

if [[ ${v66} == "1" ]]; then
 v6=`wget -qO- -6 ip.gs` 
 WARPIPv6Status=$(curl -s6 https://www.cloudflare.com/cdn-cgi/trace | grep warp | cut -d= -f2) 
 case ${WARPIPv6Status} in 
 on) 
 WARPIPv6Status=$(green "WARP is turned on, current IPV6 address：$v6 ") 
 ;; 
 off) 
 WARPIPv6Status=$(yellow "WARP isn't turned on, current IPV6 address：$v6 ") 
 esac 
else
WARPIPv6Status=$(red "IPV6 address does not exist")

 fi 
 
Print_ALL_Status_menu() {
blue "-----------------------"
blue "WGCF status\t: ${WireGuardStatus}"
blue "IPv4 status\t: ${WARPIPv4Status}"
blue "IPv6 status\t: ${WARPIPv6Status}"
blue "-----------------------"
}

if [[ ${vi} == " lxc" ]]; then
green " ---VPS Detecting---> "

else
yellow "Virtual architecture type - $vi "
yellow "This vps architecture is not supported, the script installation will automatically exit"
exit 1
fi

if [[ ${bit} == "x86_64" ]]; then

function w64(){

	if [ $release = "Debian" ]
	then
		apt install sudo -y && apt install curl sudo lsb-release iptables -y
                echo "deb http://deb.debian.org/debian $(lsb_release -sc)-backports main" | sudo tee /etc/apt/sources.list.d/backports.list
                apt update -y
                apt -y --no-install-recommends install openresolv dnsutils wireguard-tools
	elif [ $release = "Ubuntu" ]
	then
		apt-get update -y &&  apt install sudo -y
		apt -y --no-install-recommends install openresolv dnsutils wireguard-tools
	fi
wget -N -6 https://bitbucket.org/misakanolabs/euserv-warp/raw/8fc423ea7fdab554f9da26018cd3d5a3a3552fae/wgcf -O /usr/local/bin/wgcf
wget -N -6 https://bitbucket.org/misakanolabs/euserv-warp/raw/8fc423ea7fdab554f9da26018cd3d5a3a3552fae/wireguard-go -O /usr/bin/wireguard-go
chmod +x /usr/local/bin/wgcf
chmod +x /usr/bin/wireguard-go
echo | wgcf register
until [ $? -eq 0 ]
do
sleep 1s
echo | wgcf register
done
wgcf generate
sed -i 's/engage.cloudflareclient.com/2606:4700:d0::a29f:c001/g' wgcf-profile.conf
sed -i '/\:\:\/0/d' wgcf-profile.conf
sed -i 's/1.1.1.1/8.8.8.8,2001:4860:4860::8888/g' wgcf-profile.conf
cp wgcf-account.toml /etc/wireguard/wgcf-account.toml
cp wgcf-profile.conf /etc/wireguard/wgcf.conf
systemctl enable wg-quick@wgcf
systemctl start wg-quick@wgcf
rm -f wgcf* wireguard-go*
grep -qE '^[ ]*precedence[ ]*::ffff:0:0/96[ ]*100' /etc/gai.conf || echo 'precedence ::ffff:0:0/96  100' | sudo tee -a /etc/gai.conf
yellow "Check whether Warp is successfully started! \n Display IPV4 address: $(wget -qO- -4 ip.gs) "
green "If the above shows IPV4 address: 8.…………, it means success! \n If there is no IP displayed above, it means failure! !"
}

function w646(){
	    
	if [ $release = "Debian" ]
	then
		apt install sudo -y && apt install curl sudo lsb-release iptables -y
                echo "deb http://deb.debian.org/debian $(lsb_release -sc)-backports main" | sudo tee /etc/apt/sources.list.d/backports.list
                apt update -y
                apt -y --no-install-recommends install openresolv dnsutils wireguard-tools
	elif [ $release = "Ubuntu" ]
	then
		apt-get update -y &&  apt install sudo -y
		apt -y --no-install-recommends install openresolv dnsutils wireguard-tools
	fi
wget -N -6 https://bitbucket.org/misakanolabs/euserv-warp/raw/8fc423ea7fdab554f9da26018cd3d5a3a3552fae/wgcf -O /usr/local/bin/wgcf
wget -N -6 https://bitbucket.org/misakanolabs/euserv-warp/raw/8fc423ea7fdab554f9da26018cd3d5a3a3552fae/wireguard-go -O /usr/bin/wireguard-go
chmod +x /usr/local/bin/wgcf
chmod +x /usr/bin/wireguard-go
echo | wgcf register
until [ $? -eq 0 ]
do
sleep 1s
echo | wgcf register
done
wgcf generate
sed -i "5 s/^/PostUp = ip -6 rule add from $rv6 table main\n/" wgcf-profile.conf
sed -i "6 s/^/PostDown = ip -6 rule delete from $rv6 table main\n/" wgcf-profile.conf
sed -i 's/engage.cloudflareclient.com/2606:4700:d0::a29f:c001/g' wgcf-profile.conf
sed -i 's/1.1.1.1/8.8.8.8,2001:4860:4860::8888/g' wgcf-profile.conf
cp wgcf-account.toml /etc/wireguard/wgcf-account.toml
cp wgcf-profile.conf /etc/wireguard/wgcf.conf
systemctl enable wg-quick@wgcf
systemctl start wg-quick@wgcf
rm -f wgcf* wireguard-go*
grep -qE '^[ ]*precedence[ ]*::ffff:0:0/96[ ]*100' /etc/gai.conf || echo 'precedence ::ffff:0:0/96  100' | sudo tee -a /etc/gai.conf
yellow " Check whether the dual-stack Warp is successfully started (IPV4+IPV6)! \n Show IPV4 address：$(wget -qO- -4 ip.gs) Display IPV6 address：$(wget -qO- -6 ip.gs) "
green "If the above shows IPV4 address: 8.…………, IPV6 address: 2a09:…………, it means success! \n If there is no IP display in IPV4 above, and local IP in IPV6, it means failure!"
}

function w66(){

	if [ $release = "Debian" ]
	then
		apt install sudo -y && apt install curl sudo lsb-release iptables -y
                echo "deb http://deb.debian.org/debian $(lsb_release -sc)-backports main" | sudo tee /etc/apt/sources.list.d/backports.list
                apt update -y
                apt -y --no-install-recommends install openresolv dnsutils wireguard-tools
	elif [ $release = "Ubuntu" ]
	then
		apt-get update -y &&  apt install sudo -y
		apt -y --no-install-recommends install openresolv dnsutils wireguard-tools
	fi
wget -N -6 https://bitbucket.org/misakanolabs/euserv-warp/raw/8fc423ea7fdab554f9da26018cd3d5a3a3552fae/wgcf -O /usr/local/bin/wgcf
wget -N -6 https://bitbucket.org/misakanolabs/euserv-warp/raw/8fc423ea7fdab554f9da26018cd3d5a3a3552fae/wireguard-go -O /usr/bin/wireguard-go
chmod +x /usr/local/bin/wgcf
chmod +x /usr/bin/wireguard-go
echo | wgcf register
until [ $? -eq 0 ]
do
sleep 1s
echo | wgcf register
done
wgcf generate
sed -i "5 s/^/PostUp = ip -6 rule add from $rv6 table main\n/" wgcf-profile.conf
sed -i "6 s/^/PostDown = ip -6 rule delete from $rv6 table main\n/" wgcf-profile.conf
sed -i 's/engage.cloudflareclient.com/2606:4700:d0::a29f:c001/g' wgcf-profile.conf
sed -i '/0\.0\.0\.0\/0/d' wgcf-profile.conf
sed -i 's/1.1.1.1/2001:4860:4860::8888,8.8.8.8/g' wgcf-profile.conf
cp wgcf-account.toml /etc/wireguard/wgcf-account.toml
cp wgcf-profile.conf /etc/wireguard/wgcf.conf
systemctl enable wg-quick@wgcf
systemctl start wg-quick@wgcf
rm -f wgcf* wireguard-go*
grep -qE '^[ ]*label[ ]*2002::/16[ ]*2' /etc/gai.conf || echo 'label 2002::/16   2' | sudo tee -a /etc/gai.conf
yellow "Check whether Warp is successfully started! \n Display IPV6 address: $(wget -qO- -6 ip.gs) "
green "If the above shows IPV6 address: 2a09:…………, it means success! \n If the above IPV6 shows the local IP, it means failure!"
}


function cwarp(){
systemctl stop wg-quick@wgcf
systemctl disable wg-quick@wgcf
reboot
}

function owarp(){
systemctl enable wg-quick@wgcf
systemctl start wg-quick@wgcf
}

function macka(){
wget -P /root -N --no-check-certificate "https://raw.githubusercontent.com/mack-a/v2ray-agent/master/install.sh" && chmod 700 /root/install.sh && /root/install.sh
}

function phlinhng(){
curl -fsSL https://cdn.jsdelivr.net/gh/phlinhng/v2ray-tcp-tls-web@main/src/xwall.sh -o ~/xwall.sh && bash ~/xwall.sh
}

function reboot(){
reboot
}

function status(){
systemctl status wg-quick@wgcf
}

function start_menu(){
    clear
    yellow "Warp installion script Translated by MisakaNo" 
    
    red "Use bash warp-en.sh for entering this script"
    
    green "2. IPV6 only vps add warp virtual IPV4 network"
    
    green "3. IPV6 only vps add warp virtual IPV4 + IPV6 network"
    
    green "4. IPV6 only vps add warp virtual IPV6 network"
    
    green "5. Turn off WARP"
    
    green "6. Turn on WARP"
    
    green "9. Install v2ray with mack-a script"
    
    green "10. Install v2ray with phlinhng script"
    
    green "11. Reboot VPS"
    
    green "0. Exit script's menu"
    Print_ALL_Status_menu
    echo
    read -p "Enter number:" menuNumberInput
    case "$menuNumberInput" in
        2 )
           w64
	;;
        3 )
           w646
	;;
        4 )
           w66
	;;
	5 )
           cwarp
	;;
	6 )
           owarp
	;;
	9 )
           macka
	;;
	10 )
           phlinhng
	;;
	11 )
           reboot
	;;
        0 )
            exit 1
        ;;
    esac
}


start_menu "first"  


else
 yellow "Not recognized CPU"
 exit 1
fi
